"use strict";

const bcrypt = require("bcrypt");

const getHash = (password, context) => {
 // const log = context.logger.start("permit:crypto:getHash");
  const hash = bcrypt.hashSync(password, 10);
  //log.end();
  return hash;
};

const compareHash = (password, hash, context) => {
  //const log = context.logger.start("permit:crypto:compareHash");
  if (bcrypt.compareSync(password, hash)) {
    //log.end();
    return true;
  }
  //log.end();
  return false;
};

const randomString = (length =40 ) => {  
  var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}


const getNextSequenceValue = async (sequenceName) => {
   const filter = {type: sequenceName };
    const update = {$inc:{sequence_value:1}};

    let doc = await db.counter.findOneAndUpdate(filter, update);
    const counterRecord = await db.counter.findOne({ type: { $eq: sequenceName } });
    console.log("counterRecord",counterRecord);
    return counterRecord.sequence_value;
}

const setNextSequenceValue = async (sequenceName) => {
   const sequenceDocument = await new db.counter({
    type: sequenceName,
    sequence_value: 1000,
  }).save();
   return sequenceDocument;   
}

const arrayColumn = async (array, columnName) => {
    return array.map(function(value,index) {
        return value[columnName];
    })
}

exports.getNextSequenceValue = getNextSequenceValue;
exports.setNextSequenceValue = setNextSequenceValue;

exports.randomString = randomString;
exports.getHash = getHash;
exports.compareHash = compareHash;
exports.arrayColumn = arrayColumn;
