"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const user = mongoose.Schema({
    deviceToken: { type: String, required: true },
    userId: { type: String, default: "", required: true },
    uuid: { type: String, default: "", required: true },
    userName: { type: String, default: "", required: true },
    // name: { type: String, default: "", required: false },
    // email: { type: String, default: "", required: false },
    // phoneNumber: { type: String, default: "" },
    profilePic: { type: String, default: "" },
    type: { type: String, default: "U" },
    status: { type: String, default: "active" },
    coins: { type: Number, default: 60 },
    avatar: { type: String, default: "default.jpg" },
    sex: { type: String, default: "", required: false },
    // abouMe: { type: String, default: "", required: false },
    // age: { type: String, default: "", required: false },
    // jobTitle: { type: String, default: "", required: false },
    // company: { type: String, default: "", required: false },
    // education: { type: String, default: "", required: false },
    // ID: { type: String, default: "", required: false },
    lastLoggedIn: { type: Date, default: "" },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },

});

mongoose.model("user", user);
module.exports = user;