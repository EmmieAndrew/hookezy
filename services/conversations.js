'use strict'

const getOldChat = async (query, context) => {
    const log = context.logger.start('services:conversations:getOldChat')

    let pageNo = Number(query.pageNo) || 1
    let pageSize = Number(query.pageSize) || 10

    let skipCount = pageSize * (pageNo - 1)

    const chat = await db.conversation.find({ room: query.room_id }).skip(skipCount).limit(pageSize)

    chat.count = await db.conversation.find({ room: query.room_id }).count()
    log.end()
    return chat
}

const recentChats = async (query, context) => {
    const log = context.logger.start('services:conversations:recentChats')

    let pageNo = Number(query.pageNo) || 1
    let pageSize = Number(query.pageSize) || 10

    let skipCount = pageSize * (pageNo - 1)

    const chat = await db.rooms.find({$or:[{sender :query.userName},{reciever : query.userName}]}).skip(skipCount).limit(pageSize)
    const property = [];
    for (let element of chat) {
        // let unreadCount = await db.conversation.find({ msgTo: { $eq: query.userName }, readStatus: { $eq: 'Unread' } })
        // element.unreadMsgCount = unreadCount.length
        // let lastMessage = await db.conversation.find({ room: { $eq: element._id } })
        // let avtar = await db.user.findOne({ userName: { $eq: query.userName }})
        // .limit(1).sort({$natural:-1})
        // lastMessage.forEach(message => {
        //     element.lastMessage = message.msg;
        // });

        if (query.userName == element.sender) {
            let user = await db.user.findOne({ userName: { $eq: element.reciever } })
            if (user.userId !== '') {
                element.userId = user.userId
                // element.status = user.status
                // element.lastLoggedIn = user.lastLoggedIn
            } else {
                element.userId = 0
                // element.status = user.status
                // element.lastLoggedIn = user.lastLoggedIn
            }
        } else if (query.userName == element.reciever) {
            let user = await db.user.findOne({ userName: { $eq: element.sender } })
            if (user.userId !== '') {
                element.userId = user.userId
                // element.status = user.status
                // element.lastLoggedIn = user.lastLoggedIn
            } else {
                element.userId = 0
                // element.status = user.status
                // element.lastLoggedIn = user.lastLoggedIn
            }
        }
        property.push(element);
    }
    chat.count = await db.rooms.find({$or:[{sender :query.userName},{reciever : query.userName}]}).count()
    log.end()
    return chat
}

exports.getOldChat = getOldChat
exports.recentChats = recentChats

