const encrypt = require("../permit/crypto.js");
const auth = require("../permit/auth");
const path = require("path");
const { compareSync } = require("bcrypt");
const imageUrl = require('config').get('image').url
const ObjectId = require("mongodb").ObjectID;
var nodemailer = require('nodemailer')
const crypto = require("crypto");
const url = require('config').get('image').url

const build = async (model, context) => {
    const log = context.logger.start('services:users:build')
    if (!model.deviceToken || model.deviceToken === 'string') {
        throw new Error('deviceToken is required')
    }
    const user = await new db.user(model)
    log.end()
    return user

}

const setUser = (model, user, context) => {
    const log = context.logger.start("services:users:set");
    if (model.userName !== "string" && model.userName !== undefined) {
        user.userName = model.userName;
    }

    if (model.email !== "string" && model.email !== undefined) {
        user.email = model.email;
    }

    if (model.firstName !== "string" && model.firstName !== undefined) {
        user.firstName = model.firstName;
    }

    if (model.lastName !== "string" && model.lastName !== undefined) {
        user.lastName = model.lastName;
    }

    if (model.phoneNumber !== "string" && model.phoneNumber !== undefined) {
        user.phoneNumber = model.phoneNumber;
    }

    if (model.role !== "string" && model.role !== undefined) {
        user.role = model.role;
    }

    if (model.sex !== "string" && model.sex !== undefined) {
        user.sex = model.sex;
    }
    if (model.status !== "string" && model.status !== undefined) {
        user.status = model.status;
    }
    if (model.addressLine1 !== "string" && model.addressLine1 !== undefined) {
        user.addressLine1 = model.addressLine1;
    }
    if (model.addressLine2 !== "string" && model.addressLine2 !== undefined) {
        user.addressLine2 = model.addressLine2;
    }
    if (model.city !== "string" && model.city !== undefined) {
        user.city = model.city;
    }
    if (model.country !== "string" && model.country !== undefined) {
        user.country = model.country;
    }
    if (model.zipCode !== "string" && model.zipCode !== undefined) {
        user.zipCode = model.zipCode;
    }

    if (model.abouMe !== "string" && model.abouMe !== undefined) {
        user.abouMe = model.abouMe;
    }
    if (model.age !== "string" && model.age !== undefined) {
        user.age = model.age;
    }
    if (model.jobTitle !== "string" && model.jobTitle !== undefined) {
        user.jobTitle = model.jobTitle;
    }
    if (model.company !== "string" && model.company !== undefined) {
        user.company = model.company;
    }
    if (model.education !== "string" && model.education !== undefined) {
        user.education = model.education;
    }
    if (model.ID !== "string" && model.ID !== undefined) {
        user.ID = model.ID;
    }

    log.end();
    user.save();
    return user;

};

const currentUser = async(id, context) => {
    const log = context.logger.start(`services:users:currentUser`);
    let user = await db.user.findById(id);
    if (!user) {
        throw new Error("user not found");
    }
    log.end();
    return user;
};

const create = async (model, context) => {
    const log = context.logger.start('services:users:create')
    const user = await db.user.findOne({ deviceToken: { $eq: model.deviceToken } });
    // const phoneNumber = await db.user.findOne({ phoneNumber: { $eq: model.phoneNumber } });
    if (user) {
        user.deviceToken = model.deviceToken
        user.save()
        log.end()
        return user
    }
    else {
        const userName = await db.user.findOne({ userName: { $eq: model.userName } });
        if(userName){
            throw new Error("Choose another username");
        }
        const user = await build(model, context)
        log.end();
        user.save();
        return user
    }
}

const update = async(id, model, context) => {
    const log = context.logger.start(`services:users:update`);
    let entity = await db.user.findById(id);
    if (!entity) {
        throw new Error("invalid user");
    }
    if (model.email) {
        let mailExists = await db.user.findOne({ email: { $eq: model.email } });
        if(mailExists){
            throw new Error("Email Exists");
        }else{
            const user = await setUser(model, entity, context);
            log.end();
            return user
        }
    }
    const user = await setUser(model, entity, context);
    log.end();
    return user
};

const uploadProfilePic = async(id, file, context) => {
    const log = context.logger.start(`services:users:uploadProfilePic`);
    let user = await db.user.findById(id);
    console.log('file==>>>', file);
    if (!file) {
        throw new Error("image not found");
    }
    if (!user) {
        throw new Error("user not found");
    }
    if (user.avatar != "" && user.avatar !== undefined) {

        let picUrl = user.avatar.replace(`${imageUrl}`, '');
        try {
            await fs.unlinkSync(`${picUrl}`)
            console.log('File unlinked!');
        } catch (err) {
            console.log(err)
        }
    }
    const avatar = imageUrl + 'assets/images/' + file.filename
    user.avatar = avatar
    await user.save();
    log.end();
    return user
};

const randomUser = async(id, context) => {
    const log = context.logger.start(`services:users:randomUser`);
    let user = await db.user.aggregate(
        [ { $sample: { size: 1 } } ]
     )
    if (!user) {
        throw new Error("user not found");
    }
    log.end();
    return user;
};

const randomByCategory = async(id, model, context) => {
    const log = context.logger.start(`services:users:randomUser`);
    let user = await db.user.aggregate([
        { $sample: { size: 1 } },
        { $match : 
            { sex : model.category },
         }
    ])
    if (!user) {
        throw new Error("user not found");
    }
    log.end();
    return user;
};

const updateCoins = async(id, model, context) => {
    const log = context.logger.start(`services:users:updateCoins`);
    let entity = await db.user.findById(id);
    if (!entity) {
        throw new Error("invalid user");
    }
    if(entity.coins !== 0){
        let total = entity.coins;
        let sub = total - 10;
        let user = await db.user.update(
            { _id: id },
            { $set: { "coins": sub } }
         )
        if (!user) {
            throw new Error("user not found");
        }
        log.end();
        return entity;
    }else{
        throw new Error("You have not enough credits");
    }
};

const deleteUser = async(id, context) => {
    const log = context.logger.start(`services:users:deleteUser`);
    if (!id) {
        throw new Error("userId is requried");
    }
    let user = await db.user.deleteOne({ _id: id });
    if (!user) {
        throw new Error("user not found");
    }
    user.message = 'User Deleted Successfully'
    return user
};

exports.create = create
exports.update = update;
exports.uploadProfilePic = uploadProfilePic;
exports.currentUser = currentUser
exports.randomUser = randomUser;
exports.randomByCategory = randomByCategory;
exports.updateCoins = updateCoins
exports.deleteUser = deleteUser