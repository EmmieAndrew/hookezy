'use strict'
const service = require('../services/users')
const response = require('../exchange/response')
const mapper = require('../mappers/user')

const create = async(req, res) => {
    const log = req.context.logger.start(`api:users:create`);

    try {
        const user = await service.create(req.body, req.context);
        const message = "User Register Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const update = async(req, res) => {
    const log = req.context.logger.start(`api:users:update`);
    try {
        const user = await service.update(req.params.id, req.body, req.context);
        log.end();
        return response.data(res,user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const uploadProfilePic = async(req, res) => {
    const log = req.context.logger.start(`api:users:uploadProfilePic`);
    try {
        const user = await service.uploadProfilePic(req.params.id, req.file, req.context);
        const message = "Profile Picture Successfully";
        log.end();
        return response.success(res, message, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }

};

const currentUser = async (req, res) => {
    const log = req.context.logger.start(`api:users:currentUser:${req.params.id}`)

    try {
        const user = await service.currentUser(req.params.id, req.context)
        log.end()
        return response.data(res,user)
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, err.message)
    }
}

const randomUser = async (req, res) => {
    const log = req.context.logger.start(`api:users:randomUser:${req.params.id}`)

    try {
        const user = await service.randomUser(req.params.id, req.context)
        log.end()
        return response.data(res, user)
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, err.message)
    }
}

const randomByCategory = async (req, res) => {
    const log = req.context.logger.start(`api:users:randomByCategory:${req.params.id}`)

    try {
        const user = await service.randomByCategory(req.params.id, req.body,req.context)
        log.end()
        return response.data(res, user)
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, err.message)
    }
}

const updateCoins = async(req, res) => {
    const log = req.context.logger.start(`api:users:updateCoins`);
    try {
        const user = await service.updateCoins(req.params.id, req.body, req.context);
        log.end();
        return response.data(res, mapper.toModel(user));
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

const deleteUser = async(req, res) => {
    const log = req.context.logger.start(`api:users:deleteUser`);
    try {
        const user = await service.deleteUser(req.params.id, req.context);
        log.end();
        return response.data(res, user);
    } catch (err) {
        log.error(err);
        log.end();
        return response.failure(res, err.message);
    }
};

exports.currentUser = currentUser
exports.create = create
exports.update = update;
exports.uploadProfilePic = uploadProfilePic;
exports.randomUser = randomUser;
exports.randomByCategory = randomByCategory;
exports.updateCoins = updateCoins;
exports.deleteUser = deleteUser;

