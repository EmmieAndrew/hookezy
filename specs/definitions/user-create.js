module.exports = [{
    name: 'userCreate',
    properties: {
        deviceToken: {
            type: 'string'
        },
        userName: {
            type: 'string'
        },
        sex: {
            type: 'string'
        },
        userId: {
            type: 'string'
        },
        uuid: {
            type: 'string'
        },
    }
}]
