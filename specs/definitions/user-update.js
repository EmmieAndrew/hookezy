module.exports = [{
    name: "updateUser",
    properties: {
        abouMe: {
            type: "string"
        },
        age: {
            type: 'string'
        },
        jobTitle: {
            type: 'string'
        },
        company: {
            type: 'string'
        },
        education: {
            type: "string"
        },
        ID: {
            type: "string"
        },
    }
}];