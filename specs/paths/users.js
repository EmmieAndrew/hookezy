module.exports = [{
    url: '/create',
    post: {
        summary: 'create Or get',
        description: 'create Or get',
        parameters: [{
            in: 'body',
            name: 'body',
            description: 'Model of user creation',
            required: true,
            schema: {
                $ref: '#/definitions/userCreate'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },

},
{
    url: "/update/{id}",
    put: {
        summary: "update",
        description: "update",
        parameters: [{ in: "path",
                type: "string",
                name: "id",
                description: "user id",
                required: true
            },
            { in: "body",
                name: "body",
                description: "Model of user login",
                required: true,
                schema: {
                    $ref: "#/definitions/updateUser"
                }
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/currentUser/{id}",
    get: {
        summary: "currentUser",
        description: "currentUser",
        parameters: [{ in: "path",
            type: "string",
            name: "id",
            description: "user id",
            required: true
        }, ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/delete/{id}",
    delete: {
        summary: "delete",
        description: "delete",
        parameters: [{ in: "path",
            type: "string",
            name: "id",
            description: "user id",
            required: true
        }, ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/uploadProfilePic/{id}",
    put: {
        summary: "upload Profile Pic ",
        description: "upload Profile Pic ",
        parameters: [{ in: "formData",
                name: "image",
                type: "file",
                description: "The file to upload.",
                required: true,
            },
            { in: "path",
                type: "string",
                name: "id",
                description: "user id",
                required: true
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/randomUser/{id}",
    get: {
        summary: "randomUser",
        description: "randomUser",
        parameters: [{ in: "path",
            type: "string",
            name: "id",
            description: "user id",
            required: true
        }, ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/randomByCategory/{id}",
    put: {
        summary: "randomByCategory",
        description: "randomByCategory",
        parameters: [{ in: "path",
                type: "string",
                name: "id",
                description: "user id",
                required: true
            },
            { in: "body",
                name: "body",
                description: "Model of user login",
                required: true,
                schema: {
                    $ref: "#/definitions/byCategory"
                }
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
{
    url: "/updateCoins/{id}",
    put: {
        summary: "updateCoins",
        description: "updateCoins",
        parameters: [{ in: "path",
                type: "string",
                name: "id",
                description: "user id",
                required: true
            }
        ],
        responses: {
            default: {
                description: "Unexpected error",
                schema: {
                    $ref: "#/definitions/Error"
                }
            }
        }
    }
},
]
