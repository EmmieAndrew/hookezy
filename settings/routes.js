"use strict";

const fs = require("fs");
const api = require("../api");
const specs = require("../specs");
const permit = require("../permit")
const path = require("path");
const validator = require("../validators");
var multer = require('multer');

var storage = multer.diskStorage({

    destination: function(req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, path.join(__dirname, '../', 'assets'));
        } else {
            cb(null, path.join(__dirname, '../', 'assets/images'));
        }
    },
    filename: function(req, file, cb) {
        if (file.fieldname == 'csv') {
            cb(null, file.originalname);
        } else {
            cb(null, Date.now() + file.originalname);
        }
    }
});

var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });

const configure = (app, logger) => {
    const log = logger.start("settings:routes:configure");
    app.get("/specs", function(req, res) {
        fs.readFile("./public/specs.html", function(err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                });
            }
            res.contentType("text/html");
            res.send(data);
        });
    });
    app.get("/api/specs", function(req, res) {
        res.contentType("application/json");
        res.send(specs.get());
    });

    app.post(
        "/api/users/create",
        permit.context.builder,
        validator.users.create,
        api.users.create
    );

    app.put(
        "/api/users/update/:id",
        permit.context.builder,
        validator.users.update,
        api.users.update
    );

    app.put(
        "/api/users/uploadProfilePic/:id",
        permit.context.builder,
        upload.single('image'),
        api.users.uploadProfilePic
    );

    app.get(
        "/api/users/randomUser/:id",
        permit.context.userToken,
        api.users.randomUser
    );

    app.put(
        "/api/users/randomByCategory/:id",
        permit.context.userToken,
        api.users.randomByCategory
    );

    app.put(
        "/api/users/updateCoins/:id",
        permit.context.userToken,
        api.users.updateCoins
    );

    app.get(
        "/api/users/currentUser/:id",
        permit.context.builder,
        api.users.currentUser
    );

    app.delete(
        "/api/users/delete/:id",
        permit.context.builder,
        api.users.deleteUser
    );
    
    //// consversations ///

    app.get(
        '/api/conversations/getOldChat',
        permit.context.builder,
        api.conversations.getOldChat
    )

    app.get(
        "/api/conversations/recentChats",
        permit.context.builder,
        api.conversations.recentChats
    );

    log.end();
};

exports.configure = configure;