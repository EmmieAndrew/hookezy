'use strict'

exports.toModel = (entity) => {
    const model = {
        fromMsg: entity.msgFrom,
        toMsg: entity.msgTo,
        msg: entity.msg,
        date: entity.createdOn,
    }
    return model
}

exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
